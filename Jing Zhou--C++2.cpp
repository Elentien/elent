Does not completely print the matrix, but okay.
Missing report discussing experiments, accuracy, and timing. -10

#include <iostream>
#include <sstream>
#include <cmath>
#include <string>
#include <ctime>

using namespace std;

//dimension of matrix
const int N = 5;

// Jacobi iteration method
void Jacobi(int n, double a[][N], double b[N]){
	double ti = clock();
	double v[n], sum = 0.0, RMS;
	double xx[N] = {0.0,0.0,0.0,0.0,0.0};
	double x1[N] = {1.0,1.0,1.0,1.0,1.0};
	int M, iter = 0;
	string s;
	cout << "Input the number of iteration: ";
	getline(cin,s);
	if(!s.empty()){
		stringstream(s) >> M;
		for(int k=0; k<M; k++){
			for(int i=0; i<n; i++){
				int xi = 0;
				for(int j=0; j<n; j++){
					if(i != j){
						xi += a[i][j]*x1[j];
					}
				}
				x1[i] = (b[i] - xi)/a[i][i];
				if(k==M-2){
					v[i] = x1[i];
				}
				if(k==M-1){
					sum += pow(x1[i]-v[i],2);
				}
			}
		}
		RMS = sqrt(sum/n);
		cout << "iteration " << M << "\n";
		cout << "RMS " << RMS << "\n";
		cout << "x = \n";
		cout << x1[0]<<"\t"<<x1[1]<<"\t"<<x1[2]<<endl;
	}
	else{
		double error = 1.0, tol = 1e-5;
		while(error>tol){
			sum = 0.0;
			for(int i=0; i<n; i++){
				int xi = 0;
				for(int j=0; j<n; j++){
					if(i != j){
						xi +=a[i][j]*x1[j];
					}
				}
				x1[i] = (b[i] - xi)/a[i][i];
				sum += pow(x1[i]-xx[i],2);
				xx[i] = x1[i];
			}
			error = sqrt(sum/n);
			iter += 1;
		}
		cout << "iteration " << iter << "\n";
		cout << "Tolerance " << tol << "\n";
		cout << "x = \n";
		cout << x1[0]<<"\t"<<x1[1]<<"\t"<<x1[2]<<endl;
	}
	double tf = clock();
	cout << "Executing time: " << (tf-ti)*1000.0/CLOCKS_PER_SEC << endl;
}

//Gauss_Seidel method
void Gauss_Seidel(int n, double a[][N], double b[N]){
	double ti = clock();
	double v[n], y[n], sum = 0.0, RMS;
	double xx[N] = {0.0,0.0,0.0,0.0,0.0};
	double x1[N] = {1.0,1.0,1.0,1.0,1.0};
	int M, iter = 0;
	string s;
	cout << "Enter the number of iteration: ";
	getline(cin,s);
	if(!s.empty()){
		stringstream(s) >> M;
		for(int k=0; k<M; k++){
			for(int i=0; i<n; i++){
				y[i] = b[i]/a[i][i];
				for(int j=0; j<n; j++){
					if(i != j){
						y[i] -= (a[i][j]/a[i][i])*x1[j];
						x1[i] = y[i];
					}
				}
				if(k==M-2){
					v[i] = x1[i];
				}
				if(k==M-1){
					sum += pow(x1[i]-v[i],2);
				}
			}
		}
		RMS = sqrt(sum/n);
		cout << "iteration " << M << "\n";
		cout << "RMS " << RMS << "\n";
		cout << "x = \n";
		cout << x1[0]<<"\t"<<x1[1]<<"\t"<<x1[2]<<endl;
	}
	else{
		double error = 1.0, tol = 1e-5;
		while(error>tol){
			sum = 0.0;
			for(int i=0; i<n; i++){
				y[i] = b[i]/a[i][i];
				for(int j=0; j<n; j++){
					if(i != j){
						y[i] -= (a[i][j]/a[i][i])*x1[j];
						x1[i] = y[i];
					}
				}
				sum += pow(x1[i]-xx[i],2);
				xx[i] = x1[i];
			}
			error = sqrt(sum/n);
			iter += 1;
		}
		cout << "iteration " << iter << "\n";
		cout << "tolerance " << tol << "\n";
		cout << "x = \n";
		cout << x1[0]<<"\t"<<x1[1]<<"\t"<<x1[2]<<endl;
	}
	double tf = clock();
	cout << "executing time: " << (tf-ti)*1000.0/CLOCKS_PER_SEC << endl;
}

int main(){
	double A[N][N], b[N];
	//construct the matrix
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
			if(j == 0){
				A[i][j] = 1.0;
			}
			else{
				A[i][j] = 0.0;
			}
		}
		b[i] = 1.0;
	}
	// Jacobi iteration
	Jacobi(N,A,b);
	//Gauss-Seidel method
	Gauss_Seidel(N,A,b);
}

