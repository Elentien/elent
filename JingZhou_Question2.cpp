#include "stdafx.h"
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include<iomanip>
using namespace std;


int main()
{
	int n;
	cout << "Please enter a integer number represent the digit you want:  " << endl;
	cin >> n;
	if (n >= 7)
		n = 7;
	double m = 1;
	int k = 1;
	vector<double> fx;
	int i;
	double x = 0;
	double y;
	double sum;
	int j, N;
	double pi;
	cout.precision(10);
	while (m > pow(10, -n))
	{
		N = pow(10, k);
		x = 0;
		y = 1.0 / N;
		for (i = 0; i < N; i++)
		{
			fx.push_back(sqrt(1 - (pow(x, 2.0))));
			x = x + y;
		}
		sum = 0;
		for (j = 1; j < N; j++)
		{
			sum = sum + fx[j];
		}
		sum = (2.0 * sum) + fx[0];
		pi = (sum * 2.0) / N;
		m = abs(M_PI - pi);
		if (m <= pow(10, -n))
			cout << "Simulation pi is " << pi << "." << endl;
		fx.clear();
		k++;
	}
	system("PAUSE");
	return 0;
}

