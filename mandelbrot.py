import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-2,1,100)
y = np.linspace(-1.5,1.5,100)
x1,y1 = np.meshgrid(x,y)

def iteration(x,y):
    mask = np.arange(10000).reshape((100,100))
    for k in range(100):
        for i in range(100):
            z = 0
            N_max = 50;
            some_threshold = 50;
            for j in range(N_max):
                if abs(z) > some_threshold: 
                    break;
                z = z*z + x[k,i] + 1j * y[k,i]
            if j == N_max-1: mask[i,k] = 1
            else: mask[i,k] = 0
    return mask
m = iteration(x1,y1)
plt.imshow(m,extent = [-2,1,-2,2])
plt.gray()
plt.savefig('mandelbrot.png')
